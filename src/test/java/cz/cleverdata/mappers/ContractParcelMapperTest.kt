package cz.cleverdata.mappers;

import cz.cleverdata.domain.ContractParcelEntity
import cz.cleverdata.dto.ContractParcelDto
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test


class ContractParcelMapperTest {


    @Test
    fun contractDtoToContractEntity() {
        val contractParcelDto = ContractParcelDto.builder().id(1).contractId(1).parcelId(1).build();
        val contractParcelEntity: ContractParcelEntity = ContractParcelMapper.contractDtoToContractEntity(contractParcelDto);
        Assertions.assertThat(contractParcelEntity.id).isEqualTo(1);
        Assertions.assertThat(contractParcelEntity.contractId).isEqualTo(1);
        Assertions.assertThat(contractParcelEntity.parcelId).isEqualTo(1);
    }

    @DisplayName("call contractDtoToContractEntityEmpty with null")
    @Test
    fun contractDtoToContractEntityEmpty() {
        val contractParcelEntity: ContractParcelEntity? = ContractParcelMapper.contractDtoToContractEntity(null);
        Assertions.assertThat(contractParcelEntity).isEqualTo(null);
    }

    @Test
    fun contractParcelEntityToContractParcelDto() {
        val contractParcelEntity = ContractParcelEntity(2, 3, 2, null, null);
        val contractParcelDto: ContractParcelDto = ContractParcelMapper.contractParcelEntityToContractParcelDto(contractParcelEntity);
        Assertions.assertThat(contractParcelDto.id).isEqualTo(2);
        Assertions.assertThat(contractParcelDto.contractId).isEqualTo(2);
        Assertions.assertThat(contractParcelDto.parcelId).isEqualTo(3);
    }

    @DisplayName("call contractParcelEntityToContractParcelDto with null")
    @Test
    fun contractParcelEntityToContractParcelDtoEmpty() {
        val contractParcelDto: ContractParcelDto? = ContractParcelMapper.contractParcelEntityToContractParcelDto(null);
        Assertions.assertThat(contractParcelDto).isEqualTo(null);
    }
}