package cz.cleverdata.mappers;

import cz.cleverdata.domain.ParcelEntity
import cz.cleverdata.dto.ParcelDto
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test


class ParcelMapperTest {

    @Test
    fun parcelDtoToParcelEntity() {
        val parcelDto = ParcelDto.builder().id(1).ParcelSize(33).build();
        val parcelEntity: ParcelEntity = ParcelMapper.parcelDtoToParcelEntity(parcelDto);
        Assertions.assertThat(parcelEntity.id).isEqualTo(1);
        Assertions.assertThat(parcelEntity.parcelSize).isEqualTo(33);
    }

    @DisplayName("call parcelDtoToParcelEntity with null")
    @Test
    fun parcelDtoToParcelEntityEmpty() {
        val parcelEntity: ParcelEntity? = ParcelMapper.parcelDtoToParcelEntity(null);
        Assertions.assertThat(parcelEntity).isEqualTo(null);
    }

    @Test
    fun parcelEntityToParcelDto() {
        val parcelEntity = ParcelEntity(3, 44, null);
        val parcelDto: ParcelDto = ParcelMapper.parcelEntityToParcelDto(parcelEntity);
        Assertions.assertThat(parcelDto.id).isEqualTo(3);
        Assertions.assertThat(parcelDto.parcelSize).isEqualTo(44);
    }

    @DisplayName("call parcelEntityToParcelDto with null")
    @Test
    fun parcelEntityToParcelDtoEmpty() {
        val parcelEntity: ParcelEntity? = ParcelMapper.parcelDtoToParcelEntity(null);
        Assertions.assertThat(parcelEntity).isEqualTo(null);
    }
}