package cz.cleverdata.mappers;

import cz.cleverdata.domain.ContractEntity
import cz.cleverdata.dto.ContractDto
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test


class ContractMapperTest {


    @Test
    fun contractDtoToContractEntity() {
        val contractDto = ContractDto.builder().id(1).computedAllParcelSize(40).name("Contract 1").contractParcelSize(20).build();
        val contractEntity: ContractEntity = ContractMapper.contractDtoToContractEntity(contractDto);
        Assertions.assertThat(contractEntity.id).isEqualTo(1);
        Assertions.assertThat(contractEntity.name).isEqualTo("Contract 1");
        Assertions.assertThat(contractEntity.contractParcelSize).isEqualTo(20);
    }

    @Test
    @DisplayName("Test contractDtoToContractEntity with null")
    fun contractDtoToContractEntityEmpty() {
        val contractEntity: ContractEntity? = ContractMapper.contractDtoToContractEntity(null);
        Assertions.assertThat(contractEntity).isEqualTo(null);
    }

    @Test
    fun contractEntityToContractDto() {
        val contractEntity = ContractEntity(1, "Contract 5", 22, null);
        val contractDto: ContractDto = ContractMapper.contractEntityToContractDto(contractEntity);
        Assertions.assertThat(contractDto.id).isEqualTo(1);
        Assertions.assertThat(contractDto.name).isEqualTo("Contract 5");
        Assertions.assertThat(contractDto.contractParcelSize).isEqualTo(22);
    }

    @DisplayName("Test contractEntityToContractDto with null")
    @Test
    fun contractEntityToContractDtoEmpty() {
        val contractDto: ContractDto? = ContractMapper.contractEntityToContractDto(null);
        Assertions.assertThat(contractDto).isEqualTo(null);
    }
}