package cz.cleverdata.repository;

import cz.cleverdata.domain.ContractParcelEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContractParcelRepository extends JpaRepository<ContractParcelEntity, Integer> {
}
