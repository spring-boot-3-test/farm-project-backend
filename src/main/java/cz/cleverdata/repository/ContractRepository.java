package cz.cleverdata.repository;

import cz.cleverdata.domain.ContractEntity;
import cz.cleverdata.dto.InformationDtoInterface;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContractRepository extends JpaRepository<ContractEntity, Integer> {

    @Query(
            value = "select c.contract_parcel_size as parcelSize, COUNT(cp.parcel_id) as numberOfFields from contract c inner join contract_parcel cp on cp.contract_id=c.id group by c.contract_parcel_size",
            nativeQuery = true)
    List<InformationDtoInterface> getInformationForChart();

}
