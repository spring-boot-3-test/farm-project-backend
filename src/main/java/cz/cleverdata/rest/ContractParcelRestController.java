package cz.cleverdata.rest;

import cz.cleverdata.dto.ContractParcelDto;
import cz.cleverdata.service.ContractParcelService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/contractParcel")
public class ContractParcelRestController {

    private ContractParcelService contractParcelService;

    public ContractParcelRestController(ContractParcelService contractParcelService) {
        this.contractParcelService = contractParcelService;
    }

    @GetMapping("/{contractParcelId}")
    public ContractParcelDto getContractParcelById(
            @PathVariable Integer contractParcelId
    ) {
        return contractParcelService.getContractParcelById(contractParcelId);
    }

    @PostMapping
    public ContractParcelDto createContractParcel(
            @RequestBody ContractParcelDto contractDto
    ) {
        return contractParcelService.createContractParcel(contractDto);
    }

    @PutMapping("/{contractParcelId}")
    public ContractParcelDto updateContract(
            @PathVariable Integer contractParcelId,
            @RequestBody ContractParcelDto contractParcelDto
    ) {
        return contractParcelService.updateContractParcel(contractParcelId, contractParcelDto);
    }

    @DeleteMapping("/{contractParcelId}")
    public void deleteContract(
            @PathVariable Integer contractParcelId
    ) {
        contractParcelService.deleteContractParcel(contractParcelId);
    }

}
