package cz.cleverdata.rest;

import cz.cleverdata.dto.ParcelDto;
import cz.cleverdata.service.ParcelService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/parcels")
public class ParcelRestController {

    private ParcelService parcelService;

    public ParcelRestController(ParcelService parcelService) {
        this.parcelService = parcelService;
    }

    @GetMapping("/{parcelId}")
    public ParcelDto getParcelById(
            @PathVariable Integer parcelId
    ) {
        return parcelService.getContractById(parcelId);
    }

    @PostMapping
    public ParcelDto createParcel(
            @RequestBody ParcelDto contractDto
    ) {
        return parcelService.createContract(contractDto);
    }

    @PutMapping("/{parcelId}")
    public ParcelDto updateParcel(
            @PathVariable Integer parcelId,
            @RequestBody ParcelDto parcelDto
    ) {
        return parcelService.updateContract(parcelId, parcelDto);
    }

    @DeleteMapping("/{parcelId}")
    public void updateParcel(
            @PathVariable Integer parcelId
    ) {
        parcelService.deleteContract(parcelId);
    }
}
