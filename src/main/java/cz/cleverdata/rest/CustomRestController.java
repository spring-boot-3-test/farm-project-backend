package cz.cleverdata.rest;

import cz.cleverdata.dto.InformationDtoInterface;
import cz.cleverdata.service.ContractService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/custom")
public class CustomRestController {

    private ContractService contractService;

    public CustomRestController(ContractService contractService) {
        this.contractService = contractService;
    }

    /**
     * Returns some information used to create a chart in FE
     *
     * @return data for angular charts FE
     */
    @GetMapping("/getInfo")
    @CrossOrigin(origins = "http://localhost:4200")
    public List<InformationDtoInterface> getInformation(
    ) {
        return contractService.getInformationForChart();
    }

}
