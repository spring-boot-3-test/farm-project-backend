package cz.cleverdata.rest;

import cz.cleverdata.dto.ContractDto;
import cz.cleverdata.service.ContractService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/contracts")
public class ContractRestController {

    private ContractService contractService;

    public ContractRestController(ContractService contractService) {
        this.contractService = contractService;
    }

    @GetMapping("/{contractId}")
    public ContractDto getContractById(
            @PathVariable Integer contractId
    ) {
        return contractService.getContractById(contractId);
    }

    @PostMapping
    public ContractDto createContract(
            @RequestBody ContractDto contractDto
    ) {
        return contractService.createContract(contractDto);
    }

    @PutMapping("/{contractId}")
    public ContractDto updateContract(
            @PathVariable Integer contractId,
            @RequestBody ContractDto contractEntity
    ) {
        return contractService.updateContract(contractId, contractEntity);
    }

    @DeleteMapping("/{contractId}")
    public void deleteContract(
            @PathVariable Integer contractId
    ) {
        contractService.deleteContract(contractId);
    }
}
