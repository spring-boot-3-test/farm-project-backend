package cz.cleverdata.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class InformationDto implements InformationDtoInterface {
    private Integer parcelSize;
    private Integer numberOfFields;
}
