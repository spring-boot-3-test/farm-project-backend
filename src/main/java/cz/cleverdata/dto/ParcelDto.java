package cz.cleverdata.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ParcelDto {

    @Schema(accessMode = Schema.AccessMode.READ_ONLY)
    private Integer id;
    private Integer ParcelSize;
}
