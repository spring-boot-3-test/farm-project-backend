package cz.cleverdata.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ContractDto {

    @Schema(accessMode = Schema.AccessMode.READ_ONLY)
    private Integer id;
    private String name;
    @Schema(accessMode = Schema.AccessMode.READ_ONLY)
    private Integer computedAllParcelSize;
    private Integer contractParcelSize;
}
