package cz.cleverdata.dto;

public interface InformationDtoInterface {
    public Integer getParcelSize();

    public Integer getNumberOfFields();
}
