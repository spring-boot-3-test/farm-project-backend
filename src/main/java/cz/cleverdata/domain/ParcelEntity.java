package cz.cleverdata.domain;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.Collection;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "PARCEL", schema = "PUBLIC", catalog = "TESTDB")
public class ParcelEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "ID")
    private Integer id;
    @Basic
    @Column(name = "PARCEL_SIZE")
    private Integer parcelSize;
    @OneToMany(mappedBy = "parcelByParcelId")
    private Collection<ContractParcelEntity> contractParcelsById;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getParcelSize() {
        return parcelSize;
    }

    public void setParcelSize(Integer parcelSize) {
        this.parcelSize = parcelSize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ParcelEntity that = (ParcelEntity) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (parcelSize != null ? !parcelSize.equals(that.parcelSize) : that.parcelSize != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (parcelSize != null ? parcelSize.hashCode() : 0);
        return result;
    }

    public Collection<ContractParcelEntity> getContractParcelsById() {
        return contractParcelsById;
    }

    public void setContractParcelsById(Collection<ContractParcelEntity> contractParcelsById) {
        this.contractParcelsById = contractParcelsById;
    }
}
