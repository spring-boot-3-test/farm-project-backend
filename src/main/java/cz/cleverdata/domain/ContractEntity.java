package cz.cleverdata.domain;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.Collection;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "CONTRACT", schema = "PUBLIC", catalog = "TESTDB")
public class ContractEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "ID")
    private Integer id;
    @Basic
    @Column(name = "NAME")
    private String name;

    @Basic
    @Column(name = "CONTRACT_PARCEL_SIZE")
    private Integer contractParcelSize;
    @OneToMany(mappedBy = "contractByContractId")
    private Collection<ContractParcelEntity> contractParcelsById;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ContractEntity that = (ContractEntity) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (contractParcelSize != null ? !contractParcelSize.equals(that.contractParcelSize) : that.contractParcelSize != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (contractParcelSize != null ? contractParcelSize.hashCode() : 0);
        return result;
    }

    public Collection<ContractParcelEntity> getContractParcelsById() {
        return contractParcelsById;
    }

    public void setContractParcelsById(Collection<ContractParcelEntity> contractParcelsById) {
        this.contractParcelsById = contractParcelsById;
    }

    public Integer getContractParcelSize() {
        return contractParcelSize;
    }

    public void setContractParcelSize(Integer contractParcelSize) {
        this.contractParcelSize = contractParcelSize;
    }
}
