package cz.cleverdata.domain;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "CONTRACT_PARCEL", schema = "PUBLIC", catalog = "TESTDB")
public class ContractParcelEntity {
    @Basic
    @Column(name = "CONTRACT_ID")
    private Integer contractId;
    @Basic
    @Column(name = "PARCEL_ID")
    private Integer parcelId;
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "ID")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "CONTRACT_ID", referencedColumnName = "ID", nullable = false, insertable = false, updatable = false)
    private ContractEntity contractByContractId;
    @ManyToOne
    @JoinColumn(name = "PARCEL_ID", referencedColumnName = "ID", nullable = false, insertable = false, updatable = false)
    private ParcelEntity parcelByParcelId;

    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    public Integer getParcelId() {
        return parcelId;
    }

    public void setParcelId(Integer parcelId) {
        this.parcelId = parcelId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ContractParcelEntity that = (ContractParcelEntity) o;

        if (contractId != null ? !contractId.equals(that.contractId) : that.contractId != null) return false;
        if (parcelId != null ? !parcelId.equals(that.parcelId) : that.parcelId != null) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;


        return true;
    }

    @Override
    public int hashCode() {
        int result = contractId != null ? contractId.hashCode() : 0;
        result = 31 * result + (parcelId != null ? parcelId.hashCode() : 0);
        result = 31 * result + (id != null ? id.hashCode() : 0);
        return result;
    }

    public ContractEntity getContractByContractId() {
        return contractByContractId;
    }

    public void setContractByContractId(ContractEntity contractByContractId) {
        this.contractByContractId = contractByContractId;
    }

    public ParcelEntity getParcelByParcelId() {
        return parcelByParcelId;
    }

    public void setParcelByParcelId(ParcelEntity parcelByParcelId) {
        this.parcelByParcelId = parcelByParcelId;
    }
}
