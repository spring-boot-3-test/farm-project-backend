package cz.cleverdata.mappers;

import cz.cleverdata.domain.ParcelEntity;
import cz.cleverdata.dto.ParcelDto;

public class ParcelMapper {

    public static ParcelEntity parcelDtoToParcelEntity(final ParcelDto parcelDto) {
        if (parcelDto == null) {
            return null;
        }
        ParcelEntity parcelEntity = new ParcelEntity();
        parcelEntity.setParcelSize(parcelDto.getParcelSize());
        parcelEntity.setId(parcelDto.getId());
        return parcelEntity;
    }

    public static ParcelDto parcelEntityToParcelDto(final ParcelEntity parcelEntity) {
        if (parcelEntity == null) {
            return null;
        }
        ParcelDto parcelDto = new ParcelDto();
        parcelDto.setParcelSize(parcelEntity.getParcelSize());
        parcelDto.setId(parcelEntity.getId());
        return parcelDto;
    }
}
