package cz.cleverdata.mappers;

import cz.cleverdata.domain.ContractParcelEntity;
import cz.cleverdata.dto.ContractParcelDto;

public class ContractParcelMapper {

    public static ContractParcelEntity contractDtoToContractEntity(final ContractParcelDto contractParcelDto) {
        if (contractParcelDto == null) {
            return null;
        }
        ContractParcelEntity contractEntity = new ContractParcelEntity();
        contractEntity.setId(contractParcelDto.getId());
        contractEntity.setParcelId(contractParcelDto.getParcelId());
        contractEntity.setContractId(contractParcelDto.getContractId());
        return contractEntity;
    }

    public static ContractParcelDto contractParcelEntityToContractParcelDto(final ContractParcelEntity contractParcelEntity) {
        if (contractParcelEntity == null) {
            return null;
        }
        ContractParcelDto contractParcelDto = new ContractParcelDto();
        contractParcelDto.setId(contractParcelEntity.getId());
        contractParcelDto.setParcelId(contractParcelEntity.getParcelId());
        contractParcelDto.setContractId(contractParcelEntity.getContractId());
        return contractParcelDto;
    }
}
