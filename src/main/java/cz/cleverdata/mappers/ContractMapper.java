package cz.cleverdata.mappers;

import cz.cleverdata.domain.ContractEntity;
import cz.cleverdata.dto.ContractDto;

public class ContractMapper {

    public static ContractEntity contractDtoToContractEntity(final ContractDto contractDto) {
        if (contractDto == null) {
            return null;
        }
        ContractEntity contractEntity = new ContractEntity();
        contractEntity.setName(contractDto.getName());
        contractEntity.setId(contractDto.getId());
        contractEntity.setContractParcelSize(contractDto.getContractParcelSize());
        return contractEntity;
    }

    public static ContractDto contractEntityToContractDto(final ContractEntity contractEntity) {
        if (contractEntity == null) {
            return null;
        }
        ContractDto contractDto = new ContractDto();
        contractDto.setName(contractEntity.getName());
        contractDto.setId(contractEntity.getId());
        contractDto.setContractParcelSize(contractEntity.getContractParcelSize());
        return contractDto;
    }
}
