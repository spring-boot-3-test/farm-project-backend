package cz.cleverdata.service;

import cz.cleverdata.domain.ContractParcelEntity;
import cz.cleverdata.dto.ContractParcelDto;
import cz.cleverdata.mappers.ContractParcelMapper;
import cz.cleverdata.repository.ContractParcelRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ContractParcelService {

    private ContractParcelRepository contractParcelRepository;

    public ContractParcelService(ContractParcelRepository contractParcelRepository) {
        this.contractParcelRepository = contractParcelRepository;
    }

    @Transactional(readOnly = true)
    public ContractParcelDto getContractParcelById(Integer id) {
        var contractEntity = contractParcelRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("ContractParcel not found for id: " + id));
        return ContractParcelMapper.contractParcelEntityToContractParcelDto(contractEntity);
    }

    @Transactional
    public ContractParcelDto createContractParcel(ContractParcelDto contractParcelDto) {
        var contractParcelEntity = ContractParcelMapper.contractDtoToContractEntity(contractParcelDto);
        var savedEntity = contractParcelRepository.save(contractParcelEntity);
        return ContractParcelMapper.contractParcelEntityToContractParcelDto(savedEntity);
    }

    @Transactional
    public ContractParcelDto updateContractParcel(Integer id, ContractParcelDto contractParcelDto) {
        ContractParcelEntity contractParcelEntity = contractParcelRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("ContractParcel not found for id: " + id));
        return ContractParcelMapper.contractParcelEntityToContractParcelDto(contractParcelRepository.save(contractParcelEntity));
    }

    public void deleteContractParcel(Integer contractParcelId) {
        contractParcelRepository.deleteById(contractParcelId);
    }
}
