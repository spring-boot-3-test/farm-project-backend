package cz.cleverdata.service;

import cz.cleverdata.domain.ContractEntity;
import cz.cleverdata.dto.ContractDto;
import cz.cleverdata.dto.InformationDtoInterface;
import cz.cleverdata.mappers.ContractMapper;
import cz.cleverdata.repository.ContractRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ContractService {

    private ContractRepository contractRepository;

    public ContractService(ContractRepository contractRepository) {
        this.contractRepository = contractRepository;
    }

    @Transactional(readOnly = true)
    public ContractDto getContractById(Integer id) {
        var contractEntity = contractRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Contract not found for id: " + id));
        return ContractMapper.contractEntityToContractDto(contractEntity);
    }

    @Transactional
    public ContractDto createContract(ContractDto contractDto) {
        var contractEntity = ContractMapper.contractDtoToContractEntity(contractDto);
        var savedEntity = contractRepository.save(contractEntity);
        return ContractMapper.contractEntityToContractDto(savedEntity);
    }

    @Transactional
    public ContractDto updateContract(Integer id, ContractDto contractDto) {
        ContractEntity contractEntity = contractRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Contract not found for id: " + id));
        contractEntity.setName(contractDto.getName());
        return ContractMapper.contractEntityToContractDto(contractRepository.save(contractEntity));
    }

    public void deleteContract(Integer contractId) {
        contractRepository.deleteById(contractId);
    }

    public List<InformationDtoInterface> getInformationForChart() {
        return contractRepository.getInformationForChart();
    }
}
