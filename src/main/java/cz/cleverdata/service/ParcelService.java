package cz.cleverdata.service;

import cz.cleverdata.domain.ParcelEntity;
import cz.cleverdata.dto.ParcelDto;
import cz.cleverdata.mappers.ParcelMapper;
import cz.cleverdata.repository.ParcelRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ParcelService {

    private ParcelRepository parcelRepository;

    public ParcelService(ParcelRepository parcelRepository) {
        this.parcelRepository = parcelRepository;
    }

    @Transactional(readOnly = true)
    public ParcelDto getContractById(Integer id) {
        var contractEntity = parcelRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Contract not found for id: " + id));
        return ParcelMapper.parcelEntityToParcelDto(contractEntity);
    }

    @Transactional
    public ParcelDto createContract(ParcelDto contractDto) {
        var contractEntity = ParcelMapper.parcelDtoToParcelEntity(contractDto);
        var savedEntity = parcelRepository.save(contractEntity);
        return ParcelMapper.parcelEntityToParcelDto(savedEntity);
    }

    @Transactional
    public ParcelDto updateContract(Integer id, ParcelDto parcelDto) {
        ParcelEntity parcelEntity = parcelRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Parcel not found for id: " + id));
        parcelEntity.setParcelSize(parcelDto.getParcelSize());
        return ParcelMapper.parcelEntityToParcelDto(parcelRepository.save(parcelEntity));
    }

    public void deleteContract(Integer parcelId) {
        parcelRepository.deleteById(parcelId);
    }
}
