drop table IF EXISTS CONTRACT_PARCEL;
drop table IF EXISTS CONTRACT;
drop table IF EXISTS PARCEL;

CREATE TABLE contract
(
    id                   INT AUTO_INCREMENT PRIMARY KEY,
    name                 VARCHAR(100),
--                           computed_all_parcel_size INT,
    contract_parcel_size INT,
    UNIQUE (name)
);

CREATE TABLE parcel
(
    id          INT AUTO_INCREMENT PRIMARY KEY,
    parcel_size INT
);

CREATE TABLE contract_parcel
(
    contract_id INT NOT NULL REFERENCES contract (id) ON DELETE CASCADE,
    parcel_id   INT NOT NULL REFERENCES parcel (id) ON DELETE CASCADE,
    id          INT AUTO_INCREMENT PRIMARY KEY
);

INSERT INTO contract(name, contract_parcel_size)
VALUES ('Contract 1', 100);
INSERT INTO contract(name, contract_parcel_size)
VALUES ('Contract 2', 140);
INSERT INTO contract(name, contract_parcel_size)
VALUES ('Contract 3', 200);
INSERT INTO contract(name, contract_parcel_size)
VALUES ('Contract 4', 150);
INSERT INTO contract(name, contract_parcel_size)
VALUES ('Contract 5', 400);
INSERT INTO contract(name, contract_parcel_size)
VALUES ('Contract 6', 800);
INSERT INTO contract(name, contract_parcel_size)
VALUES ('Contract 7', 300);
INSERT INTO parcel(parcel_size)
VALUES (20);
INSERT INTO parcel(parcel_size)
VALUES (40);
INSERT INTO parcel(parcel_size)
VALUES (60);
INSERT INTO parcel(parcel_size)
VALUES (80);
INSERT INTO parcel(parcel_size)
VALUES (1);
INSERT INTO parcel(parcel_size)
VALUES (2);
INSERT INTO parcel(parcel_size)
VALUES (3);
INSERT INTO parcel(parcel_size)
VALUES (4);
INSERT INTO parcel(parcel_size)
VALUES (5);
INSERT INTO parcel(parcel_size)
VALUES (40);
INSERT INTO parcel(parcel_size)
VALUES (88);
INSERT INTO parcel(parcel_size)
VALUES (8);
INSERT INTO parcel(parcel_size)
VALUES (10);
INSERT INTO parcel(parcel_size)
VALUES (8);
INSERT INTO parcel(parcel_size)
VALUES (6);
INSERT INTO contract_parcel(contract_id, parcel_id)
VALUES (1, 1);
INSERT INTO contract_parcel(contract_id, parcel_id)
VALUES (1, 2);
INSERT INTO contract_parcel(contract_id, parcel_id)
VALUES (2, 3);
INSERT INTO contract_parcel(contract_id, parcel_id)
VALUES (2, 4);
INSERT INTO contract_parcel(contract_id, parcel_id)
VALUES (2, 5);
INSERT INTO contract_parcel(contract_id, parcel_id)
VALUES (3, 5);
INSERT INTO contract_parcel(contract_id, parcel_id)
VALUES (3, 6);
INSERT INTO contract_parcel(contract_id, parcel_id)
VALUES (4, 7);
INSERT INTO contract_parcel(contract_id, parcel_id)
VALUES (5, 8);
INSERT INTO contract_parcel(contract_id, parcel_id)
VALUES (5, 9);
INSERT INTO contract_parcel(contract_id, parcel_id)
VALUES (5, 10);
INSERT INTO contract_parcel(contract_id, parcel_id)
VALUES (6, 11);
INSERT INTO contract_parcel(contract_id, parcel_id)
VALUES (7, 12);
INSERT INTO contract_parcel(contract_id, parcel_id)
VALUES (7, 13);
INSERT INTO contract_parcel(contract_id, parcel_id)
VALUES (7, 14);
INSERT INTO contract_parcel(contract_id, parcel_id)
VALUES (7, 15);
